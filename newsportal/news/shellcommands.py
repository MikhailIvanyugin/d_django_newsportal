# Ниже приведены комманды в Python shell

# 1
# user_andrey = User.objects.create_user(username='Андрей')
# user_sergey = User.objects.create_user(username='Сергей')

# 2
# Author.objects.create(authorUser=user_andrey)
# Author.objects.create(authorUser=user_sergey)

# 3
# Category.objects.create(name='Новости Петербурга')
# Category.objects.create(name='Транспорт')
# Category.objects.create(name='Строительство')
# Category.objects.create(name='Промышленность')

# 4
# author_andrey = Author.objects.get(id=1)
# author_sergey = Author.objects.get(id=2)
#
# Post.objects.create(author=author_andrey, categoryType='NW',
# title='В Петербурге стартовал третий этап транспортной реформы',
# text='трудности с поставкой газовых автобусов, которые являются одним из главных нововведений
# транспортной реформы')
#
# Post.objects.create(author=author_andrey, categoryType='AR',
# title='Российские автобусы на газе',
# text='В России научились делать вполне современные, оптимальные по конструкции газовые автобусы')
#
# Post.objects.create(author=author_sergey, categoryType='AR',
# title='Сколько АГНКС в России?',
# text='Сейчас метановых заправок в стране около 540')

# 5
# Post.objects.get(id=1).postCategory.add(Category.objects.get(id=1))
# Post.objects.get(id=1).postCategory.add(Category.objects.get(id=2))
# Post.objects.get(id=2).postCategory.add(Category.objects.get(id=3))
# Post.objects.get(id=3).postCategory.add(Category.objects.get(id=4))

# 6
# Comment.objects.create(commentPost=Post.objects.get(id=1), commentUser=Author.objects.get(id=1).authorUser, text='Скорее бы')
# Comment.objects.create(commentPost=Post.objects.get(id=1), commentUser=Author.objects.get(id=2).authorUser, text='Не дождешься как обычно')
# Comment.objects.create(commentPost=Post.objects.get(id=2), commentUser=Author.objects.get(id=1).authorUser, text='Даешь газ!')
# Comment.objects.create(commentPost=Post.objects.get(id=3), commentUser=Author.objects.get(id=2).authorUser, text='Очень мало')

# 7
# Comment.objects.get(id=1).like() - 10 раз
# Comment.objects.get(id=1).rating - рейтинг 10
# Comment.objects.get(id=2).dislike() - 5 раз
# Comment.objects.get(id=4).like() - 2 раза
# Comment.objects.get(id=5).dislike() - 1 раз
# Post.objects.get(id=1).like() - 3 раза
# Post.objects.get(id=2).like() - 1 раз
# Post.objects.get(id=3).like() - 5 раз

# 8
# Author.objects.get(id=1).update_rating()
# Author.objects.get(id=1).ratingAuthor  результат 24 (9+3+10+2)
# Author.objects.get(id=2).update_rating()
# Author.objects.get(id=2).ratingAuthor  результат 9 (15-5-1)

# 9
# most_popular_user = Author.objects.order_by('-ratingAuthor')[:1]
# for i in most_popular_user:
# ...     i.ratingAuthor
# ...     i.authorUser.username

# 10
# best_post = Post.objects.order_by('-rating')[:1]
# for i in best_post:
# ...     i.dateCreation
# ...     i.author.authorUser.username
# ...     i.rating
# ...     i.title
# ...     i.preview()

# 11
# best_post = Post.objects.order_by('-rating')[0].comment_set.all()
# for i in best_post:
# ...     i.commentPost.title
# ...     i.commentUser
# ...     i.text
# ...     i.dateCreation
# ...     i.rating
#
