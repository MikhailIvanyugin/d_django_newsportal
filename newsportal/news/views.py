from django.shortcuts import render

from .models import *

# class PostList(ListView):
#     model = Post
#     ordering = 'dateCreation'
#     template_name = 'postlist.html'
#     context_object_name = 'post_list'

def index(request):
    post_list = Post.objects.order_by('-dateCreation')
    return render(request, 'index.html', context={'post_list': post_list})

